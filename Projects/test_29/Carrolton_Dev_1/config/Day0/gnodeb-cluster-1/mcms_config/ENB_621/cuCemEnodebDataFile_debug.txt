MOD:vbbu/enodeb/Name==mav_VBBU1
MOD:vbbu/enodeb/enbId==621
MOD:vbbu/enodeb/PCI_LIST[1]==415
MOD:vbbu/enodeb/CipheringAlgorithmList==AES_ALGO
MOD:vbbu/enodeb/IntegrityAlgorithmList==AES_ALGO
MOD:vbbu/enodeb/S1LossCellImpactTimer==5
MOD:vbbu/enodeb/lr_ENB_ENDC_CFG==ENB_ENDC_CFG1
MOD:vbbu/enodeb/lr_OppQciMap[1]==OppQciMap1
MOD:vbbu/enodeb/lr_OppQciMap[2]==OppQciMap5
MOD:vbbu/enodeb/lr_OppQciMap[3]==OppQciMap9
MOD:vbbu/enodeb/RSRPCounterThldlowIndex==-100
MOD:vbbu/enodeb/RSRPCounterThldupIndex==-60
MOD:vbbu/enodeb/PUSCHSINRThld==5
MOD:vbbu/enodeb/RSRPSINRMeas==false
MOD:vbbu/enodeb/ulPathlossThr==100
MOD:vbbu/enodeb/RSRPPathLossMeas==false
MOD:vbbu/enodeb/maxCellsSupported==1
MOD:vbbu/enodeb/lr_CUConfig==mav_CU1
MOD:vbbu/MMEInterface/MMEPoolNumberOfEntries==1
DEL:vbbu/MMEInterface/lr_MMEPool[2]
DEL:vbbu/MMEPool[2]
DEL:vbbu/Mme[2]
DEL:vbbu/supportedTAs[2]
DEL:vbbu/PLMNList[2]
REP:vbbu/MMEPool[1]
REP:vbbu/Mme[1]
REP:vbbu/supportedTAs[1]
REP:vbbu/PLMNList[1]
MOD:vbbu/MMEInterface/lr_MMEPool[1]==MmePool_VBBU1
MOD:vbbu/MMEPool[2]/Name==MmePool_VBBU1
MOD:vbbu/MMEPool[2]/mmePoolId==1
MOD:vbbu/MMEPool[2]/lr_supportedTAs[1]==TA_VBBU1
MOD:vbbu/supportedTAs[2]/Name==TA_VBBU1
MOD:vbbu/supportedTAs[2]/TAC==1
MOD:vbbu/supportedTAs[2]/lr_PLMNList[1]==PLMNList_1
MOD:vbbu/PLMNList[2]/Name==PLMNList_1
MOD:vbbu/PLMNList[2]/PLMNID==11111
MOD:vbbu/MMEPool[2]/MMENumberOfEntries==1
MOD:vbbu/MMEPool[2]/lr_Mme[1]==SCTP_VBBU1
MOD:vbbu/Mme[2]/Name==SCTP_VBBU1
MOD:vbbu/Mme[2]/MMEId==1
MOD:vbbu/Mme[2]/MMEIp==172.16.60.99
DEL:vbbu/MMEPool[1]
DEL:vbbu/Mme[1]
DEL:vbbu/supportedTAs[1]
DEL:vbbu/PLMNList[1]
MOD:vbbu/CUConfig/Name==mav_CU1
MOD:vbbu/CUConfig/CUId==1
MOD:vbbu/CUConfig/CUName==MAV_CU1
MOD:vbbu/CUConfig/duplexMode==FDD
MOD:vbbu/CUConfig/numDUSupported==1
MOD:vppcfg/dpdk/dev[1]/name==v1u0
MOD:vppcfg/dpdk/dev[2]/name==s1u0
MOD:vppcfg/dpdk/dev[1]/interface==EP_V1U
MOD:vppcfg/dpdk/dev[2]/interface==EP_S1U
MOD:vppcfg/custom[1]/vppctl==set cu-up-ip-bypass enable
MOD:vppcfg/custom[2]/vppctl==set cu-up-log level 5
MOD:vbbu/OppQciMap[1]/Name==OppQciMap1
MOD:vbbu/OppQciMap[1]/OppQci==11
MOD:vbbu/OppQciMap[1]/MapStdQci==1
MOD:vbbu/OppQciMap[2]/Name==OppQciMap5
MOD:vbbu/OppQciMap[2]/OppQci==15
MOD:vbbu/OppQciMap[2]/MapStdQci==5
MOD:vbbu/OppQciMap[3]/Name==OppQciMap9
MOD:vbbu/OppQciMap[3]/OppQci==19
MOD:vbbu/OppQciMap[3]/MapStdQci==9
MOD:vbbu/LOG_CFG/rrcLogMask==0x0h
MOD:vbbu/Features_enb/IgnoreMMEGroupIdFromUE==false
MOD:vbbu/Features_enb/ENDCSupported==3
MOD:vbbu/Features_enb/drbOverlapAction==INTRA_CELL_HO
MOD:vbbu/ENB_ENDC_CFG/Name==ENB_ENDC_CFG1
MOD:vbbu/ENB_ENDC_CFG/MaxENDCX2Peers==5
MOD:vbbu/ENB_ENDC_CFG/X2ApProcDefenseTimer==6000
MOD:vbbu/ENB_ENDC_CFG/retryTimerForSctpX2Cxn==5
MOD:vbbu/PLMNList/PLMNID==44020
MOD:vbbu/ENB_ENDC_CFG/retryTimerForSctpX2Cxn==5
